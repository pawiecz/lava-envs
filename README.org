* LAVA environments
** Introduction
*** Notes
[[https://gitlab.collabora.com/pawiecz/lava-envs][https://gitlab.collabora.com/pawiecz/lava-envs]]
*** Covered use cases
**** Quick start
- The fewer commands the better
- Preferred /industry standard/ tools and conventions
- Low entry cost
**** Virtual device
- Minimize /moving parts/
- Low maintenance cost
- Availability: no requirements for specific hardware
*** Suggestions?
Open a PR to this repository!
** Upstream docker-compose
*** Repository
[[https://git.lavasoftware.org/lava/pkg/docker-compose][https://git.lavasoftware.org/lava/pkg/docker-compose]]
*** Demo
- Prebuilt images
- Makefile targets for convenience (but not really required)

#+BEGIN_SRC sh
# edit .env or use the one from config/docker-compose directory in this repository
docker-compose up
# or from scratch:
make
#+END_SRC

Go to [[http://localhost][http://localhost]]
**** Post-deployment
0. (Ensure there is a superuser: edit =.env=)
1. Add known device-type
2. Add new device
3. Set device dictionary
4. Set device-type health-check
*** Suggested use cases
- Getting familiar with just the LAVA components
- Specific device needs (no defaults; all populated manually)
- Verifying features/issues with upstream (e.g. db management)
  - Rebuild and substitute image
  - Add configuration overlays
*** Future work
- Convenience (optional) targets for adding sample virtual device
- Support images with LAVA installed from deb package
** Collabora-flavored =ci-run=
*** Repository
[[https://gitlab.collabora.com/lava/lava/-/blob/collabora/production/docker/collabora/ci-run][https://gitlab.collabora.com/lava/lava/-/blob/collabora/production/docker/collabora/ci-run]]
*** Demo
- Prebuilt images
- [[https://gitlab.collabora.com/lava/lava/-/blob/collabora/production/docker/collabora/README.md][Well documented]]
- Straightforward wrappers (definitely worth reviewing)
- *Proper* =ci-run= (there are two!)

#+BEGIN_SRC sh
# from main directory:
docker/collabora/ci-run skip-cleanup skip-build
# or from scratch:
docker/collabora/ci-run skip-cleanup
# halt:
docker-compose -f docker/collabora/docker-compose.yaml -f docker/collabora/docker-compose.ci.yaml stop
#+END_SRC

Go to [[http://localhost:8000][http://localhost:8000]]
*** Suggested use cases
- Ephemeral local testing environment (e.g. CI)
- Validating patches for Collabora's lab staging environment
*** Future work
- Separate containers for LAVA services?
- Revisit [[https://gitlab.collabora.com/pawiecz/automated-testing-lab-notes/-/tree/main/local][automated-testing-lab-notes]] and ensure useful information is migrated
** KernelCI lava-docker
*** Repository
[[https://github.com/kernelci/lava-docker][https://github.com/kernelci/lava-docker]]
*** Demo
- Prebuilt environment configuration and images
- [[https://github.com/kernelci/lava-docker/blob/master/README.md][Extensive documentation]]

#+BEGIN_SRC sh
# edit boards.yaml or use the one from config/lava-docker directory in this repository
# from output/local:
docker-compose up
# or from scratch:
./lavalab-gen.sh # destructive: removes =output= directory
# then proceed with generated docker-compose
docker-compose build # and up
#+END_SRC

Go to [[http://localhost:8888][http://localhost:8888]]
*** Suggested use cases
- Local environment with configuration management
- Make sure required use cases are supported
- Unavailable KVM (disabled instead of adding devices and capabilities)
*** Future work
- Submit documentation fixes
- Revisit =lavalab-gen.py= (still /gets job done/)
** Summary
- No "one size fits all" environment
- Manage expectations, balance trade-offs
- Get familiar with various deployment types
*** Personal recommendation
| Use case            | Environment                 | Trade-off       |
|---------------------+-----------------------------+-----------------|
| Quick test          | Collabora-flavored =ci-run= | Flexibility     |
| Local environment   | KernelCI lava-docker        | Feature support |
| Service interaction | Upstream docker-compose     | Manual tuning   |
| Debugging           | Local installation on a VM  | (not covered)   |
